import Head from "next/head";
import styled from "styled-components";

const SingleItemStyles = styled.div`
  max-width: 1200px;
  margin: 2rem auto;
  box-shadow: ${(props) => props.theme.bs};
  display: grid;
  grid-auto-columns: 1fr;
  grid-auto-flow: column;
  min-height: 800px;
  img {
    width: 100%;
    height: 100%;
    object-fit: contain;
  }
  .details {
    margin: 3rem;
    font-size: 2rem;
  }
`;

const SingleItem = () => {
  return (
    <SingleItemStyles>
      <Head>
        <title>Sick Fits | title</title>
      </Head>
      <img src="https://via.placeholder.com/180x300" alt="" />
      <div className="details">
        <h2>Viewing</h2>
        <p>description</p>
      </div>
    </SingleItemStyles>
  );
};

export default SingleItem;
