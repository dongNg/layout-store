import Head from "next/head";
import Link from "next/link";
import PaginationStyles from "../styles/PaginationStyles";

const Pagination = () => {
  return (
    <PaginationStyles>
      <Head>
        <title> Sick Fits! — Page 1 of 2</title>
      </Head>
      <Link href="/">
        <a className="prev">Pre</a>
      </Link>
      <p>
        Page 1 of <span className="totlaPages">2</span>
      </p>
      <p>1 Items Total</p>
      <Link href="/">
        <a className="next">Next</a>
      </Link>
    </PaginationStyles>
  );
};

export default Pagination;
