import Link from "next/link";
import OrderItemStyles from "../styles/OrderItemStyles";
import styled from "styled-components";

const OrderUl = styled.ul`
  display: grid;
  grid-gap: 4rem;
  grid-template-columns: repeat(auto-fit, minmax(40%, 1fr));
`;
const OrderList = () => {
  return (
    <div>
      <h2>You have</h2>
      <OrderUl>
        <OrderItemStyles>
          <Link href="/orders/1">
            <a>
              <div className="order-meta">
                <p>1 Items</p>
                <p>1 Products</p>
                <p> about 2 hours</p>
                <p>$990.90</p>
              </div>
              <div className="images">
                <img src="https://via.placeholder.com/150" alt="" />
              </div>
            </a>
          </Link>
          <Link href="/orders/2">
            <a>
              <div className="order-meta">
                <p>1 Items</p>
                <p>1 Products</p>
                <p> about 2 hours</p>
                <p>$990.90</p>
              </div>
              <div className="images">
                <img src="https://via.placeholder.com/150" alt="" />
                <img src="https://via.placeholder.com/150" alt="" />
              </div>
            </a>
          </Link>
        </OrderItemStyles>
      </OrderUl>
    </div>
  );
};

export default OrderList;
