import CartStyles from "../styles/CartStyles";
import CloseButton from "../styles/CloseButton";
import SickButton from "../styles/SickButton";
import Supreme from "../styles/Supreme";
import CartItem from "./CartItem";

const Cart = (props) => {
  const handleClose = (): void => {
    props.handleOpen(false);
  }
  return (
    <CartStyles {...props} open={props.open}>
      <header>
        <CloseButton title="close" onClick={() => handleClose()}>&times;</CloseButton>
        <Supreme>Your Cart</Supreme>
        <p>You Have __ Items in your cart.</p>
      </header>
      <ul>
        <CartItem></CartItem>
      </ul>
      <footer>
        <p>Money</p>
        <SickButton>Checkout</SickButton>
      </footer>
    </CartStyles>
  );
};

export default Cart;
