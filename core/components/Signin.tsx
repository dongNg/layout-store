import Form from "../styles/Form";
const Signin = () => {
  return (
    <Form>
      <fieldset>
        <h2>Sign into your account</h2>
        <label htmlFor="email">
          Email
          <input type="email" name="email" placeholder="Email" />
        </label>
        <label htmlFor="password">
          Password
          <input type="password" name="password" placeholder="Password" />
        </label>
        <button type="submit">Sign In!</button>
      </fieldset>
    </Form>
  );
};

export default Signin;
