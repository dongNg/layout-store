import styled from "styled-components";
import Item from "./Item";
import Pagination from "./Pagination";
const Center = styled.div`
  text-align: center;
`;

const ItemsList = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 60px;
  max-width: ${(props) => props.theme.maxWidth};
  margin: 0 auto;
`;

const Items = () => {
  return (
    <Center>
      <Pagination />
      <ItemsList>
        <Item />
        <Item />
        <Item />
        <Item />
      </ItemsList>
      <Pagination />
    </Center>
  );
};

export default Items;
