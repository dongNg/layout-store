import styled from "styled-components";
import RemoveFromCart from "./RemoveFromCart";

const CartItemStyles = styled.li`
  padding: 1rem 0;
  border-bottom: 1px solid ${(props) => props.theme.lightgrey};
  display: grid;
  align-items: center;
  grid-template-columns: auto 1fr auto;
  img {
    margin-right: 10px;
  }
  h3,
  p {
    margin: 0;
  }
`;

const CartItem = () => {
  return (
    <CartItemStyles>
      <img width="100" src="https://via.placeholder.com/150" alt="" />
      <div className="cart-item-details">
        <h3>Tilte</h3>
        <p>
          price * quantity {" - "}
          <em>quantity</em>
        </p>
      </div>
      <RemoveFromCart />
    </CartItemStyles>
  );
};

export default CartItem;
