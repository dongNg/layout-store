import Head from "next/head";
import OrderStyles from "../styles/OrderStyles";

const Order = () => {
  return (
    <OrderStyles>
      <Head>
        <title>Sick Fits - Order id</title>
      </Head>
      <p>
        <span>Order ID:</span>
        <span>id</span>
      </p>
      <p>
        <span>Charge</span>
        <span>charge</span>
      </p>
      <p>
        <span>Date</span>
        <span>createdAt</span>
      </p>
      <p>
        <span>Order Total</span>
        <span>total</span>
      </p>
      <p>
        <span>Item Count</span>
        <span>items - length</span>
      </p>
      <div className="items">
        <div className="order-item">
          <img src="https://via.placeholder.com/150" alt="image" />
          <div className="item-details">
            <h2>title</h2>
            <p>Qty: quantity</p>
            <p>Each: price</p>
            <p>SubTotal: price * quantity</p>
            <p>item.description</p>
          </div>
        </div>
      </div>
    </OrderStyles>
  );
};

export default Order;
