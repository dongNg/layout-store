import Head from "next/head";

const Meta = () => {
  return (
    <Head>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link rel="icon" href="/favicon.ico" />
      <link rel="stylesheet" href="/nprogress.css"/>
      <title>Store</title>
    </Head>
  );
};

export default Meta;
