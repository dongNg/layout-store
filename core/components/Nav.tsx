import Link from "next/link";
import NavStyles from "../styles/NavStyles";
import CartCount from "./CartCount";

const Nav = (props) => {
  const handleOpen = (val): void => {
    props.handleOpen(true);
  };
  return (
    <NavStyles datatype="nav">
      <Link href="/items">
        <a>Shop</a>
      </Link>
      <Link href="/sell">
        <a>Sell</a>
      </Link>
      <Link href="/orders">
        <a>Orders</a>
      </Link>
      <Link href="/signup">
        <a>Account</a>
      </Link>
      <button onClick={() => handleOpen(true)}>
        My Cart
        <CartCount count={0}></CartCount>
      </button>
    </NavStyles>
  );
};

export default Nav;
