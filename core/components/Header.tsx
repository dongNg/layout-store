import Link from "next/link";
import Router from "next/router";
import styled from "styled-components";
import Nav from "./Nav";
import NProgress from "nprogress";
import Cart from "./Cart";
import { useState } from "react";
import { EventEmitter } from "events";

Router.events.on("routeChangeStart", (url) => {
  console.log(`Loading: ${url}`);
  NProgress.start();
});
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

const Logo = styled.h1`
  font-size: 4rem;
  margin-left: 2rem;
  position: relative;
  z-index: 2;
  transform: skew(-7deg);
  a {
    padding: 0.5rem 1rem;
    background: ${(props) => props.theme.red};
    color: white;
    text-transform: uppercase;
    text-decoration: none;
  }
  @media (max-width: 1300px) {
    margin: 0;
    text-align: center;
  }
`;

const StyleHeader = styled.header`
  .bar {
    border-bottom: 10px solid ${(props) => props.theme.black};
    display: grid;
    grid-template-columns: auto 1fr;
    justify-content: space-between;
    align-items: stretch;
    @media (max-width: 1300px) {
      grid-template-columns: 1fr;
      justify-content: center;
    }
  }
  .sub-bar {
    display: grid;
    grid-template-columns: 1fr auto;
    border-bottom: 1px solid ${(props) => props.theme.lightgrey};
  }
`;

const Header = () => {
  const [open, setOpen] = useState(false);
  const handleOpen = (val) => {
    setOpen(val);
  };
  return (
    <StyleHeader>
      <div className="bar">
        <Logo>
          <Link href="/">
            <a>Sick Fits</a>
          </Link>
        </Logo>
        <Nav handleOpen={(event) => handleOpen(event)} />
      </div>
      <div className="sub-bar">
        <p>Search</p>
      </div>
      <Cart open={open} handleOpen={(event) => handleOpen(event)} />
    </StyleHeader>
  );
};

export default Header;
