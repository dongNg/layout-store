import Link from "next/link";
import ItemStyles from "../styles/ItemStyles";
import PriceTag from "../styles/PriceTag";
import Title from "../styles/Title";
const Item = () => {
  return (
    <ItemStyles>
        <img src="https://via.placeholder.com/150" alt=""/>
      <Title>
        <Link href="/items/1">
          <a>Title</a>
        </Link>
      </Title>
      <PriceTag>Price</PriceTag>
      <p>Description</p>
      <div className="buttonList">
        <Link href="/">
          <a>Edit</a>
        </Link>
        <Link href="/">
          <a>Add To Cart</a>
        </Link>
        <Link href="/">
          <a>Delete Item</a>
        </Link>
      </div>
    </ItemStyles>
  );
};

export default Item;
