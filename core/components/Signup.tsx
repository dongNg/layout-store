import Form from "../styles/Form";
const Signup = () => {
  return (
    <Form>
      <fieldset>
        <h2>Sign Up for An Account</h2>
        <label htmlFor="email">
          Email
          <input type="email" name="email" placeholder="Email" />
        </label>
        <label htmlFor="name">
          Name
          <input type="text" name="name" placeholder="name" />
        </label>
        <label htmlFor="password">
          Password
          <input type="password" name="password" placeholder="Password" />
        </label>
        <button type="submit">Sign In!</button>
      </fieldset>
    </Form>
  );
};

export default Signup;
