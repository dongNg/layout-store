import Items from "../../core/components/Items"

const ItemPage = () => {
    return (
        <div>
            <Items />
        </div>
    )
}

export default ItemPage;
