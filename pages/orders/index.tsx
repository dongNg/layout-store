import OrderList from "../../core/components/OrderList"

const OrderPage = () => {
    return (
        <div>
            <OrderList />
        </div>
    )
}

export default OrderPage;
